#!/bin/bash
set +x

ENV_LOCATION=$1
DISABLE_TXT=$2
UPDATE_TXT=$3
ENABLE_TXT=$4
ADD_TXT=$5
BACKUP_LOCATION=$6
PROJECT_NAME=$7

add_env="add"
disable_env="disable"
update_env="update"
enable_env="enable"

# BACKUP EXISTING .ENV
backup(){
  TIME=$(date +%d-%m-%Y_%H:%M:%S)
  cp "${ENV_LOCATION}" "${BACKUP_LOCATION}/${PROJECT_NAME}_env_${TIME}"
}

# CHECK jumlah_field => $1 = $ADD | $DISABLE | $UPDATE | $ENABLE ; $2 = function_name ; $3 = $add_env | $disable_env | $update_env | $enable_env
check(){
  JUMLAH_FIELD=$(awk -F "#" '{print NF}' $1)
  if [ $JUMLAH_FIELD -ne 0 -a "$2" == "add" ]; then
    echo -en "\n" >> $ENV_LOCATION
    $2
  elif [ $JUMLAH_FIELD -ne 0 ]; then
    $2
  else
    echo "Tidak perlu ${3} vars" 
  fi
}

# ADD ENV FUNCTION
add() {
  JUMLAH_FIELD=$(awk -F "#" '{print NF}' $ADD_TXT)
  x=1
  while [ $x -le $JUMLAH_FIELD ]
  do
    ENV_VAR="$(awk -v field_num="$x" -F "#" '{print $field_num}' $ADD_TXT)"
    grep -q $ENV_VAR $ENV_LOCATION
    if [ "$?" == 1 ]; then
      echo $ENV_VAR >> $ENV_LOCATION
    else
      echo "$ENV_VAR already exists at $ENV_LOCATION"
    fi
    x=$(( $x + 1 ))
  done
}

# UPDATE ENV FUNCTION 
update() {
  JUMLAH_FIELD=$(awk -F "#" '{print NF}' $UPDATE_TXT)
  x=1
  y=2
  while [ $y -le $JUMLAH_FIELD ]
  do
    sed -i "s/$(awk -v field_num="$x" -F "#" '{print $field_num}' $UPDATE_TXT)/$(awk -v field_num="$y" -F "#" '{print $field_num}' $UPDATE_TXT)/g" $ENV_LOCATION
    x=$(( $x + 2 ))
    y=$(( $y + 2 ))
  done
}


# DISABLE ENV FUNCTION 
disable(){
  JUMLAH_FIELD=$(awk -F "#" '{print NF}' $DISABLE_TXT)
  x=1
  while [ $x -le $JUMLAH_FIELD ]
  do
    sed -i "s/$(awk -v field_num="$x" -F "#" '{print $field_num}' $DISABLE_TXT)/#$(awk -v field_num="$x" -F "#" '{print $field_num}' $DISABLE_TXT)/g" $ENV_LOCATION
    x=$(( $x + 1 ))
  done
}

# ENABLE ENV FUNCTION
enable(){
  JUMLAH_FIELD=$(awk -F "#" '{print NF}' $ENABLE_TXT)
  x=1
  while [ $x -le $JUMLAH_FIELD ]
  do
    sed -i "s/#$(awk -v field_num="$x" -F "#" '{print $field_num}' $ENABLE_TXT)/$(awk -v field_num="$x" -F "#" '{print $field_num}' $ENABLE_TXT)/g" $ENV_LOCATION
    x=$(( $x + 1 ))
  done
}

# FORMAT ADJUSTMENT FUNCTION => $1 = $DISABLE_TXT | $UPDATE_TXT | $ENABLE_TXT 
format_adjustment(){
  format_adjustment_1 $1
  format_adjustment_2 $1
  format_adjustment_3 $1
}

format_adjustment_1(){ #untuk slash ('/')
  CHARACTER=('/')  
  for character in "${CHARACTER[@]}";
  do
    sed -i "s/\\$character/\\\\\\$character/g" $1
  done
}

format_adjustment_2(){ # => untuk backthick ('`')
    sed -i "s/\`/\\\\\`/g" $1
}

format_adjustment_3(){ # untuk => '$' '.' '*' '[' ']' '^' '!' '&' '"'
  CHARACTER=('$' '.' '*' '[' ']' '^' '!' '&' '"')  
  for character in "${CHARACTER[@]}"; 
  do
    sed -i "s/\\$character/\\\\$character/g" $1
  done
}

# EXECUTE
backup

format_adjustment $UPDATE_TXT
format_adjustment $DISABLE_TXT
format_adjustment $ENABLE_TXT

check $UPDATE_TXT   update  $update_env
check $DISABLE_TXT  disable  $disable_env
check $ENABLE_TXT   enable $enable_env
check $ADD_TXT      add     $add_env


