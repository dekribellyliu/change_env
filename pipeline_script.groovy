/*
parameter => ENV_LOCATION
             PROJECT_NAME
             DISABLE_ENV
             UPDATE_ENV
             ENABLE_ENV
             ADD_ENV
*/

def remote = [:]
remote.name = "<whatever you want>"
remote.host = "<your server ip address>" 
remote.allowAnyHosts = true

def TXT_DIR = "/root/txt_file/${PROJECT_NAME}"
def ADD_TXT = "${TXT_DIR}/add.txt"
def DISABLE_TXT = "${TXT_DIR}/disable.txt"
def UPDATE_TXT = "${TXT_DIR}/update.txt"
def ENABLE_TXT = "${TXT_DIR}/enable.txt"
def BACKUP_LOCATION = "/root/backup_env/${PROJECT_NAME}"

node {
    withCredentials([sshUserPrivateKey(credentialsId: 'staging-ssh-key', keyFileVariable: 'identity', passphraseVariable: 'passphrase', usernameVariable: 'userName')]) {
        remote.user = userName
        remote.identityFile = identity
        remote.passphrase = passphrase
        stage("Pull script") {
            sshCommand remote: remote, command: 'git clone git@bitbucket.org:dekribellyliu/change_env.git || true ' + '&& cd change_env ' + '&& git checkout master ' + '&& git pull origin master '
        }
        stage("Write PARAMETER into a txt file") {
            sh "echo '$ADD_ENV' > add.txt"
            sh "echo '$DISABLE_ENV' > disable.txt"
            sh "echo '$UPDATE_ENV' > update.txt"
            sh "echo '$ENABLE_ENV' > enable.txt"
            sh "ls -lah"
        }
        stage("Upload txt files") {
            sshCommand remote: remote, command: "mkdir -p ${TXT_DIR}"
            sshPut remote: remote, from: 'add.txt', into: "${ADD_TXT}"
            sshPut remote: remote, from: 'disable.txt', into: "${DISABLE_TXT}"
            sshPut remote: remote, from: 'update.txt', into: "${UPDATE_TXT}"
            sshPut remote: remote, from: 'enable.txt', into: "${ENABLE_TXT}"                                                                         
        }
        stage("Run Script"){
            sshCommand remote: remote, command: "mkdir -p ${BACKUP_LOCATION}"
            sshCommand remote: remote, command: "sh /root/change_env/script.sh" + 
              ' ' + ENV_LOCATION +
              ' ' + DISABLE_TXT +
              ' ' + UPDATE_TXT +
              ' ' + ENABLE_TXT +
              ' ' + ADD_TXT +
              ' ' + BACKUP_LOCATION +
              ' ' + PROJECT_NAME
        }
        stage("Confirmation"){
            try{
              def confirmDialog = "Apakah perubahan .env sudah sesuai?"
              def releaseApprover = input message: confirmDialog,ok: "Yes",submitterParameter: "releaseApprover"
              echo "Change .env success"
            } 
            catch(e){
              sshCommand remote: remote, command: "sh /root/change_env/rollback.sh" +
              ' ' + ENV_LOCATION +
              ' ' + BACKUP_LOCATION
              echo "Rollback .env success"
            }
        }
    }
}
